package project;

import java.net.Socket;


public class Main {
	
	private final String HOST = "localhost";
	private final int PORT = 1147;

	private Socket sock;
	
	public static void main(String[] args) {
		
		System.out.println("Elevator Controller Started!");
	
		Main controller = new Main();
		controller.initialize();
	}

	private void initialize() {
		
		try {
			sock = new Socket(HOST, PORT);
		} catch (Exception e) {
			System.err.println("An error occured while opening up the connection!");
			System.err.print(e.toString());
		}
		
	}

}
