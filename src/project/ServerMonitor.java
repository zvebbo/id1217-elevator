package project;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ServerMonitor extends Thread{

	// This is where we read data from
	BufferedReader in;
	
	// Pipe to server
	PrintWriter out;
	
	// This is the buffer of commands we fill
	List<String> buffer;

	// Timeout between reads
	private final int TIMEOUT = 20;
	
	boolean DEBUG = true;
	
	public ServerMonitor(BufferedReader in, PrintWriter out) {
		this.out = out;
		this.in = in;
		this.buffer = new ArrayList<String>(50);
	}

	@Override
	public void run() {
		String line = "";
		
		while(true) {
			try {
				line = in.readLine();
				System.out.println("ServerMonitor queued command: " + line);
				this.putCommand(line);
				//Thread.currentThread().sleep(TIMEOUT);
			} catch (Exception e) {
				System.out.println("Error in ServerMonitor.run(), terminating.");
				e.printStackTrace();
				System.exit(1);
			}
		}
	}

	/**
	 * Adds a command to the end of the buffer.
	 * @param s - The command to add
	 */
	private synchronized void putCommand(String s) {
		buffer.add(s);
		this.notifyAll();
	}

	/**
	 * Returns the command in the head of the buffer.
	 */
	public synchronized String getCommand() {
		while(buffer.isEmpty()) {
			try {
				this.wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return buffer.remove(0);
	}
	
	/**
	 * Sends a command to the server.
	 * @param s - The command to send
	 */
	public synchronized void sendCommand(String s) {
		if(DEBUG)
			System.out.println("ServerMonitor: Sending command: " + s);
		out.println(s);
		out.flush();
	}
}
