
package project;


public class Elevator extends Thread{

	ServerMonitor serverMonitor;
	
	private double yPosition = 0;
	private static double velocity; // The current velocity of the elevator
	private int myID;
	private int direction; // the current direction of the elevator, ie if its moving up or down
	private int queued_direction;  // the next desired direction of the elevator. 0 means doesn't matter
	private int visitFloor[][]; // an array of [visit_or_not, queued_direction]
	private int floorIndicator;
	private static final double FAULT_TOLERANCE = 0.04; // How close we have to be to the floor
	private boolean emergencyStop = false; // true if emergency stop has been pressed

	public Elevator(int myID, ServerMonitor serverMonitor, int floors){
		this.myID = myID;
		this.serverMonitor = serverMonitor;
		visitFloor = new int[floors][2];
	}
	
	@Override
	public void run(){
		
		// While this elevator is running, check if there are any floors we should visit, if so, visit them.
		while(true) {
			int[][] floors = this.getVisitFloor();
			for(int i = 0; i < floors.length; i++) {
				if (floors[i][0] == 1) {
					goToFloor(i, floors[i][1]);
					floors[i][0] = 0;
				}
			}
		}
		
	}

	/** 
	 * Open the doors, keep them open for a short while, and then
	 * close them again.
	 */
	private void openDoors() {
		serverMonitor.sendCommand("d " + myID + " 1");
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		serverMonitor.sendCommand("d " + myID + " -1");
		try {
			sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Make this elevator, when it wants to, to visit a specific floor.
	 * @param floor - the floor to visit
	 * @param queued direction - the direction to continue afterward
	 */
	public synchronized void visitFloor(int floor, int qd) {
		visitFloor[floor][0] = 1;
		visitFloor[floor][1] = qd;
		
		if(MainController.DEBUG){
			System.out.println("floor: " + floor);
			System.out.println("[0]: " + visitFloor[floor][0]);
			System.out.println("[1]: " + visitFloor[floor][1]);
		}
	}
	
	/**
	 * Returns the array of floors this elevator might need to visit.
	 */
	public synchronized int[][] getVisitFloor() {
		return this.visitFloor;
	}
	
	/**
	 * Tell this elevator to move either upwards or downwards.
	 * Sends this command to the server through ServerMonitor.
	 * @param floor - eg 3
	 */
	private void goToFloor(int destination, int qd){
		if(MainController.DEBUG)
			System.out.println("goToFloor(" + destination + ", " + qd + ")");
	
		// panel click, not sure where to go next
		this.queued_direction = qd;
		
		if(yPosition == destination){ //Elevator already at that floor, don't move.
			direction = 0;
		}
		else if(yPosition > destination){ // Elevator must move down
			direction = -1;
		}
		else{ //Elevator must move up
			direction = 1;
		}
		serverMonitor.sendCommand("m " + myID + " " + direction);
		
		// Continously check the elevators position to see whether or not
		// it has reached the destination. 
		double distance;
		int vf[][];
		do {
			distance = Math.abs(this.getYPosition() - destination);

			if ((int)Math.round(this.getYPosition()) != floorIndicator) {
				floorIndicator = (int)Math.round(this.getYPosition());
				serverMonitor.sendCommand("s " + this.myID + " " + floorIndicator);
			}
		
			// Do a pit-stop (stop on the way to destination) if the passing floor is marked that it needs to be visited
			// and the direction is either 0 (don't care) or the same as the elevators direction.
			vf = this.getVisitFloor();
			if(vf[floorIndicator][0] == 1 && 
			   vf[floorIndicator][1] != -1*direction &&
			   floorIndicator != destination) {
				doPitStop(floorIndicator);
			}
			
			if(emergencyStop){
				break;
			}
		} while (distance > FAULT_TOLERANCE);
		
		if(!emergencyStop){
			System.out.println("floor reached!");
			serverMonitor.sendCommand("m " + myID + " " + 0); //stop
			openDoors();
			direction = 0;
			queued_direction = 0;
		}
		else{
			System.out.println("Emergency Stop was pressed, stopping.");
			serverMonitor.sendCommand("m " + myID + " " + 0); //stop
			for(int i = 0; i < visitFloor.length; i++){ // clear this elevators visit queue
				visitFloor[i][0] = 0;
				visitFloor[i][1] = 0;
			}
			emergencyStop = false;
		}
		
		if(MainController.DEBUG) {
			System.out.println("Elevator " +myID + " has reached its destination: " + yPosition);
		}
	}
	
	private void doPitStop(int pitStopDestination) {
		double distance = Double.MAX_VALUE;
		do {
			distance = Math.abs(this.getYPosition() - pitStopDestination);
		} while (distance > FAULT_TOLERANCE);
		
		System.out.println("floor reached (pit-stopped)!");
		visitFloor[pitStopDestination][0] = 0;
		visitFloor[pitStopDestination][1] = 0;
		serverMonitor.sendCommand("m " + myID + " " + 0); // stop
		this.openDoors();
		serverMonitor.sendCommand("m " + myID + " " + direction); // continue
	}

	/**
	 * Set this elevator's y position, i.e the floor this elevator
	 * is currently on.
	 * @param yPos y coordinate of the elevator
	 */
	public synchronized void setYPosition(double yPos) {
		this.yPosition = yPos;
	}
	
	/**
	 * Retrieve the current y position of this elevator.
	 * @return - y position, eg 3.2
	 */
	public synchronized double getYPosition() {
		return yPosition;
	}

	/**
	 * Given a command, this will return an estimation of how much work it would be
	 * for this elevator to perform this task.
	 * @param buff
	 */
	public double estimateCost(String buff) {
		double cost = 0;
		
		if (buff.startsWith("b ")) {
			int floor = 0;
			double dir = 0;
			floor = CommandHelper.getFloor(buff);
			dir = CommandHelper.getValue(buff);
			
			cost += Math.abs(floor - this.getYPosition());
		
			System.out.println("dir: " + dir);
			System.out.println("direction: " + direction);
			System.out.println("queued direction: " + queued_direction);
			
			// If user want to go down, and elevator is moving up, double the cost
			if (dir == -queued_direction) {
				cost *= 2;
			}
		}
		
		if(MainController.DEBUG) {
			System.out.println("Elevator " + this.myID + " estimates " + buff + " to " + cost);
		}
		
		return cost;
	}
	
	/**
	 * Sets the velocity of this elevator, when the velocity changes
	 * @param buff change velocity command
	 */
	public static synchronized void setVelocity(String buff){
		velocity = CommandHelper.getVelocity(buff);
		
		if(MainController.DEBUG) {
			System.out.println("Setting velocity: " + velocity);
		}
	}
	
	/**
	 * Sets the value of emergencyStop to true.
	 */
	public void setEmergencyStop(){
		emergencyStop = true;
	}
}

