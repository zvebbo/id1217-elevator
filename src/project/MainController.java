package project;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class MainController {
	
	public static final boolean DEBUG = true;

	// Configuration variables
	private final String HOST = "localhost";
	private final int PORT = 4711;

	private Socket sock;
	private BufferedReader in;
	private PrintWriter out;
	private int numFloors;
	private int numElevators;
	private Thread elevators[];
	
	// Reads from a buffer
	private ServerMonitor serverMonitor;
	
	// Timeout between ticks
	private final int TIMEOUT = 20;
	
	/**
	 * Program entry point.
	 */
	public static void main(String[] args) {
		int numFloors = args.length > 1 ? Integer.parseInt(args[0]) : 6;
		int numElevators = args.length > 1 ? Integer.parseInt(args[1]) : 2;
		
		System.out.println("Elevator Controller Started!");
	
		MainController controller = new MainController(numFloors, numElevators);
		controller.initialize();
		controller.run();
		controller.teardown();
	}

	/**
	 * Default constructor for the main controller.
	 * @param numFloors
	 * @param numElevators
	 */
	public MainController(int numFloors, int numElevators) {
		this.numFloors = numFloors;
		this.numElevators = numElevators;
	}

	/**
	 * Start the MainController
	 */
	private void run() {
		System.out.println("Main event loop for MainControlling has begun");
		String buff;
		int elevatorNr;
		double value;
		Elevator tempElevator = null;
		//BufferedReader terminalIn = in = new BufferedReader(new InputStreamReader(System.in)); 
		
		while(true) {
			buff = serverMonitor.getCommand();
			
			if(DEBUG) {
				System.out.println("read " + buff);
			}
			
			if(buff == null) {
				System.out.println("Terminating");
				System.exit(0);
			}
			
			elevatorNr = CommandHelper.getElevator(buff);
			value = CommandHelper.getValue(buff);
			
			// Get elevator from array of elevator threads
			if(elevatorNr != -1){
				tempElevator = (Elevator) elevators[elevatorNr];
			}
		
			// An elevator has moved
			if(buff.startsWith("f ")) {
				tempElevator.setYPosition(value);
			}
		
			// Someone pushed a button inside the elevator
			if(buff.startsWith("p ")) {
				if(buff.endsWith("32000")){
					tempElevator.setEmergencyStop();
				}
				else{
					tempElevator.visitFloor((int)value, 0);
				}
			}
		
			// Someone puashed a floor button, eg. im on floor 5 and i want to go down
			if(buff.startsWith("b ")) {
				Elevator el = null;
			
				// Find the best/cheapest elevator for this command
				double minCost = Double.MAX_VALUE;
				double temp;
				int min_index = -1;
				for(int i = 0; i < elevators.length; i++) {
					el = (Elevator)elevators[i];
					
					if ((temp = el.estimateCost(buff)) < minCost) {
						minCost = temp;
						min_index = i;
					}
				}
			
				// Issued to cheapest elevator to visit floor.
				el = (Elevator)elevators[min_index];
				el.visitFloor(CommandHelper.getFloor(buff), (int)CommandHelper.getValue(buff));
				if(DEBUG){
					System.out.println("visitFloor(" + CommandHelper.getFloor(buff) + 
										", " + (int)CommandHelper.getValue(buff));
				}
			}
			
			if(buff.startsWith("v ")){
				tempElevator.setVelocity(buff);
			}
		}
	}

	/**
		Sets up the sockets. Elevator workers etc.
	 */
	private void initialize() {
		
		try {
			sock = new Socket(HOST, PORT);
			in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			out = new PrintWriter(sock.getOutputStream());
			
			if (sock.isConnected()) {
				System.out.println("Successfully connected to elevator service.");
			}
		} catch (Exception e) {
			System.err.println("An error occured while opening up the connection! Terminating.");
			e.printStackTrace();
			System.exit(1);
		}
	
		// Set up server monitor
		serverMonitor = new ServerMonitor(in, out);
		serverMonitor.start();
	
		// Create elevators
		elevators = new Elevator[numElevators];
		for(int i = 0; i < numElevators; i++) {
			elevators[i] = new Elevator(i + 1, serverMonitor, numFloors);
			elevators[i].start();
		}
	}
	
	/**
	 * Tears down a controllers. Like close the sockets etc.
	 */
	private void teardown() {
		System.out.print("Tearing down...");
		try {
			out.close();
			in.close();
			sock.close();
		} catch (IOException e) {
			System.err.println("Something crashed while tearing down the controller.");
			e.printStackTrace();
		}
		System.out.println(" done!");
	}
}
