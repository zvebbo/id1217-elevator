package project;

public class CommandHelper {
	
	/**
	 * Returns which elevator this command is for.
	 * @param command A command string, eg f 1 0.0800
	 * @return The elevator that this command concerns, eg 1 or -1 if none.
	 */
	public static int getElevator(String command) {
		if(MainController.DEBUG)
			System.out.println("CommandHelper. Command: " + command);
		
		if (command.charAt(0) == 'f') {
			return Integer.parseInt(command.substring(2, 3)) - 1; // elevators array is 0 indexed
		}
		else if(command.charAt(0) == 'p'){
			return Integer.parseInt(command.substring(2, 3)) - 1; 
		}
		
		return -1;
	}

	/**
	 * Returns which floor that this command works upon.
	 * @param command string
	 * @return floor - eg 3
	 */
	public static int getFloor(String command) {
		if(command.charAt(0) == 'b'){
			return Integer.parseInt(command.substring(2, 3)); 
		}
		
		return -1;
	}
	
	/**
	 * Returns the value of this command.
	 * @param buff
	 * @return
	 */
	public static double getValue(String buff) {
		return Double.parseDouble( buff.substring(buff.lastIndexOf(' ')) );
	}

	/**
	 * Returns the velocity value of a command.
	 * @param buff
	 * @return
	 */
	public static double getVelocity(String buff){
		return Double.parseDouble(buff.substring(buff.lastIndexOf(' ')));
	}
}
